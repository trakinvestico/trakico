pragma solidity 0.4.18;

import './StandardToken.sol';

contract TrakToken is StandardToken {
    // FIELDS
    string constant public  name = "TrakInvest Token" ;
    string constant public  symbol = "TRAK";
    uint256 constant public  decimals = 18;

    // The flag indicates if the crowdsale contract is in Funding state.
    bool public fundraising = true;

    // who created smart contract
    address public creator;
    // owns the total supply of tokens - it would be DAO
    address public tokensOwner;
    mapping (address => bool) public frozenAccounts;

  /// events
    event FrozenFund(address target ,bool frozen);

  /// modifiers

    modifier isCreator() { 
      require(msg.sender == creator);  
      _; 
    }

    modifier onlyPayloadSize(uint size) {
        assert(msg.data.length >= size + 4);
        _;
    }


    modifier onlyOwner() {
        require(msg.sender == tokensOwner);
        _;
    }

    modifier manageTransfer() {
        if (msg.sender == tokensOwner) {
            _;
        }
        else {
            require(fundraising == false);
            _;
        }
    }

  /// constructor
    function TrakToken(
      address _fundsWallet,
      uint256 initialSupply
      ) public {
      creator = msg.sender;

      if (_fundsWallet !=0) {
        tokensOwner = _fundsWallet;
      }
      else {
        tokensOwner = msg.sender;
      }

      totalSupply = initialSupply * (uint256(10) ** decimals);
      balances[tokensOwner] = totalSupply;
      Transfer(0x0, tokensOwner, totalSupply);
    }


  /// overriden methods

    function transfer(address _to, uint256 _value)  public manageTransfer onlyPayloadSize(2 * 32) returns (bool success) {
      require(!frozenAccounts[msg.sender]);
      require(_to != address(0));
      return super.transfer(_to, _value);
    }

    function transferFrom(address _from, address _to, uint256 _value)  public manageTransfer onlyPayloadSize(3 * 32) returns (bool success) {
      require(!frozenAccounts[msg.sender]);
      require(_to != address(0));
      require(_from != address(0));
      return super.transferFrom(_from, _to, _value);
    }


    function freezeAccount (address target ,bool freeze) public onlyOwner {
      frozenAccounts[target] = freeze;
      FrozenFund(target,freeze);  
    }

    function burn(uint256 _value) public onlyOwner returns (bool burnSuccess) {
        require(fundraising == false);
        return super.burn(_value);
    }

    /// @param newAddress Address of new owner.
    function changeTokensWallet(address newAddress) public onlyOwner returns (bool)
    {
        require(newAddress != address(0));
        tokensOwner = newAddress;
    }

    function finalize() public  onlyOwner {
        require(fundraising != false);
        // Switch to Operational state. This is the only place this can happen.
        fundraising = false;
    }


    function() public {
        revert();
    }

}



