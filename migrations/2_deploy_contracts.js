var token = artifacts.require("./TrakToken.sol");
var crowdSale = artifacts.require("./CrowdSale.sol");

module.exports = function(deployer) {
  deployer.deploy(token ,"0x64fdbd158713fa7517625bbe12bbd12dae5941ad",100);
  deployer.deploy(crowdSale);
};
