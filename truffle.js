// module.exports = {
//   networks: {
//     development: {
//       host: "localhost",
//       port: 8545,
//       network_id: "*" // Match any network id
//     }
//     // ,
//     // rinkeby: {
//     //   host: "localhost", // Connect to geth on the specified
//     //   port: 8545,
//     //   from: "0x470e3720992E9b4A839260FD95F2E9af872C1B41", // default address to use for any transaction Truffle makes during migrations
//     //   network_id: 4,
//     //   gas: 1218835 // Gas limit used for deploys
//     // }
//   }
// };


module.exports = {
    rpc: {
        host: "localhost",
        port: 8545
    },
    networks: {
        development: {
            host: "localhost",
            port: 8545,
            network_id: "*"
        },
        testnet: {
            host: "localhost",
            port: 8545,
            network_id: 3,
            gas: 2000000
        },
        live: {
            host: "localhost",
            port: 8545,
            network_id: 1,  // Ethereum public network
            gas: 4000000,
            gasPrice: 20000000000
        }
    }
};

